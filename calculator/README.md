# calculator
　ブラウザ上で動く簡単な計算機[^1]。こののかにある`index.html`を開くとブラウザ上で計算機が起動します。

- **[`index.html`](index.html)** <br>
    コンテンツの配置、表示を設定などのレイアウトを設定
- **[`style.css`](style.css)** <br>
    各レイアウトの装飾を設定
- **[`script.js`](script.js)** <br>
    バックエンド処理を実装

[^1]:[参考元](https://webukatu.com/wordpress/blog/27277/)